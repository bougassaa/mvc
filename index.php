<?php
session_start();

require_once 'Autoloader.php'; // Autoloading of PHP classes, to avoid include on each files
require_once 'function.php';

display_errors();

$router = new Router();
$router->process();