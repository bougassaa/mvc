{extends file="Modules/Utils/Views/template.tpl"}

{block "body"}
    <div class="page-not-found">
        <div class="container">
            <div class="jumbotron">
                <div class="container text-center">
                    <h1 class="display-4">{$msg}</h1>
                    <a class="btn btn-primary mt-4 btn-sm" href="javascript:window.history.back()" role="button">{t('LBL_GO_BACK')}</a>
                </div>
            </div>
        </div>
    </div>
{/block}
