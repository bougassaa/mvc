<nav class="navbar navbar-expand-lg navbar-dark bg-primary">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarCollapse">
        <a class="navbar-brand" href="{Configuration::get('webRoot')}">{t('LBL_TITLE_HOME')}</a>
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-link" href="#">Item 1</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Item 2</a>
            </li>
            <li class="nav-item dropdown">
                <a class="nav-link dropdown-toggle" id="navbarDropdown" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    {t('LBL_LANGUAGE')}
                </a>
                <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="Home/language/setLanguage/fr">Français</a>
                    <a class="dropdown-item" href="Home/language/setLanguage/en">English</a>
                </div>
            </li>
        </ul>

        <form class="form-inline" action="Search" method="post">
            <input class="form-control mr-sm-2" name="word" type="search" placeholder="{t('LBL_SEARCH')}" aria-label="{t('LBL_SEARCH')}" required>
            <button class="btn btn-outline-light my-2 my-sm-0" type="submit">{t('LBL_SEARCH')}</button>
        </form>
    </div>
</nav>