<?php


class HomeController extends Controller
{

    protected function index()
    {
        $this->view->postView();
    }

    protected function language()
    {
        $language = $this->request->get('setLanguage');
        $_SESSION['language'] = $language;

        redirection('Home');
    }
}