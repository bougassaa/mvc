<?php

function display_errors()
{
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

function t($key)
{
    $language = Session::getCurrentLanguage();
    $file = "Language/$language/string.php";

    if(!file_exists($file)) {
        throw new Exception("You have to create file for strings and set used language");
    }

    include $file;

    /** @var array $string_values -> string.php */
    return isset($string_values[$key]) ? $string_values[$key] : $key;
}

function log2($data)
{
    file_put_contents('log.txt', print_r($data, true).PHP_EOL, FILE_APPEND);
}

function redirection($module, $action = '', $params = '')
{
    header("Location: " . Configuration::get('webRoot') . "$module/$action" . ($params ? "/$params" : ""));
}