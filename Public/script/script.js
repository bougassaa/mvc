// create generic function for ajax, from jQuery
function myAjax(model, action, params, func = null) {
    $.post(model + '/' + action, params).done(data => { if(func != null) func(data) });
}