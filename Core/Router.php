<?php


class Router
{

    public function process()
    {
        try {
            $request = new Request();
            $this->callController($request);
        } catch (Exception $e) {
            $this->createError($e);
        }
    }

    /**
     * @param Request $request
     * @throws Exception
     */
    public function callController(Request $request)
    {
        $module = $request->getModule();
        $action = $request->get('action');

        $module = $module ?: Configuration::get('default_module'); // get default module from configuration
        $action = $action ?: 'index'; // set default action if no action

        $className = $module . 'Controller';

        if(!class_exists($className)) {
            throw new Exception(t('LBL_NO_CONTROLLER'));
        }

        /** @var Controller $controller */
        $controller = new $className();
        $controller->setRequest($request);
        $controller->setModule($module);
        $controller->callAction($action);
    }

    private function createError(Exception $e)
    {
        $errorView = View::getView('Utils', 'error');
        $errorView->setTitle(t('LBL_TITLE_ERROR'));
        $errorView->assign('msg', $e->getMessage());
        $errorView->postAjaxView();
    }
}