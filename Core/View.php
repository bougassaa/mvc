<?php

require_once('Libraries/Smarty/Smarty.class.php');

class View
{
    private $title = '';
    private $templateName;
    private $module;
    public $smarty;

    /**
     * View constructor.
     * @param $module
     * @param $viewName
     */
    public function __construct($module, $viewName)
    {
        $smarty = new Smarty();
        $smarty->setTemplateDir("Modules/$module/Views");
        $smarty->setCompileDir('Smarty/templates_c');
        $smarty->setConfigDir('Smarty/configs');

        $this->smarty = $smarty;
        $this->templateName = $viewName;
        $this->module = $module;
    }

    public function setTitle($title)
    {
        $this->title = $title;

        if($this->smarty) {
            $this->smarty->assign('title', $title);
        }
    }

    /**
     * @param $module string
     * @param $viewName string
     * @return View|null
     */
    public static function getView($module, $viewName)
    {
        $link = "Modules/$module/Views/$viewName.tpl";

        if(file_exists($link)) {
            $view = new self($module, $viewName);
        } else {
            $view = null;
        }

        return $view;
    }

    /**
     * @return string
     * @throws Exception
     */
    public function generateView()
    {
        if($this->templateName) {
            return $this->smarty->fetch($this->templateName . '.tpl');
        } else {
            throw new Exception(t('LBL_NO_VIEW'));
        }
    }

    public function postAjaxView($includeJs = false)
    {
        if($includeJs && $this->getJsForModule()) {
            $this->assign('script', $this->getJsForModule());
        }

        echo $this->generateView();
    }

    public function postView()
    {
        $content = $this->generateView();

        // create view for template
        $template = self::getView('Utils', 'template');

        $template->setTitle($this->title);
        $template->assign('content', $content);

        if($this->getJsForModule()) {
            $template->assign('script', $this->getJsForModule());
        }

        echo $template->generateView();
    }

    public function assign($key, $value)
    {
        $this->smarty->assign($key, $value);
    }

    private function getJsForModule()
    {
        $script = false;
        $file = 'Public/script/' . strtolower($this->module) . '.js';

        if(file_exists($file)) {
            $script = $file;
        }

        return $script;
    }
}