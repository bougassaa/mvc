<?php


class Request extends Core
{

    public function __construct()
    {
        $this->setDatas(array_merge($_GET, $_POST, $_SESSION));
    }

    /**
     * @param $key string
     * @return boolean
     */
    public function paramExist($key)
    {
        return isset($this->datas[$key]);
    }

    public function getModule()
    {
        return $this->get('module');
    }
}