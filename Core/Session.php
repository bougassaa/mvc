<?php


class Session
{
    static $connectedKey = 'connected_user'; // todo: set key for connected user as you want

    public static function isConnectedUser()
    {
        return isset($_SESSION[static::$connectedKey]) ? $_SESSION[static::$connectedKey] : false;
    }

    public static function connectUser($id = true)
    {
        $_SESSION[static::$connectedKey] = $id;
    }

    public static function disconnect()
    {
        session_unset();
        session_destroy();
    }

    public static function getCurrentLanguage()
    {
        return Session::get('language') ?: Configuration::get('default_language');
    }

    public static function get($key)
    {
        return isset($_SESSION[$key]) ? $_SESSION[$key] : null;
    }
}