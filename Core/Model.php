<?php


abstract class Model
{
    private static $bdd;
    protected $businessClass = "Core";

    /**
     * @return PDO
     * @throws Exception
     */
    private static function getBdd() {
        if (self::$bdd === null) {
            // Get parameter from config file
            $dsn   = Configuration::get("dsn");
            $login = Configuration::get("login");
            $pwd   = Configuration::get("pwd");

            // Create connection
            try {
                self::$bdd = new PDO($dsn, $login, $pwd, [PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION]);
            } catch (PDOException $e) {
                throw new Exception(t('LBL_CONNECTION_FAILED') . $e->getMessage());
            }
        }

        return self::$bdd;
    }

    /**
     * @param $sql
     * @param null $params
     * @return PDOStatement
     */
    protected function query($sql, $params = null) {
        if ($params == null) {
            $resultat = self::getBdd()->query($sql);
        }
        else {
            $resultat = self::getBdd()->prepare($sql);
            $resultat->execute($params);
        }

        return $resultat;
    }

    /**
     * @param $res PDOStatement
     * @return Core[]
     */
    protected function fetchToObject($res)
    {
        $objects = [];
        foreach ($res->fetchAll() as $record) {
            /** @var Core $obj */
            $obj = new $this->businessClass();
            $obj->setDatas($record);
            $objects[] = $obj;
        }

        return $objects;
    }

    /**
     * @return integer
     */
    protected function lastInsertId(){
        return self::getBdd()->lastInsertId();
    }
}