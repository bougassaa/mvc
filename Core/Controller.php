<?php


abstract class Controller
{
    /**
     * @var Request
     */
    protected $request;

    /**
     * @var View
     */
    protected $view;

    private $module;

    /**
     * @param Request $request
     */
    public function setRequest($request)
    {
        $this->request = $request;
    }

    public function setModule($module)
    {
        $this->module = $module;
    }

    /**
     * @param $action string
     * @throws Exception
     */
    public function callAction($action)
    {
        if(!method_exists($this, $action)) {
            throw new Exception(t('LBL_ACTION_NOT_EXIST'));
        } else {
            // get view if exist for this action
            $view = View::getView($this->module, $action);

            if($view) {
                // find title for view from module name
                $view->setTitle(t(strtoupper('LBL_TITLE_' . $this->module)));
                $this->view = $view;
            }

            //calling the action
            $this->$action();
        }
    }

    protected abstract function index();
}