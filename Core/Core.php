<?php


class Core
{

    protected $datas = [];

    public function get($key)
    {
        return (isset($this->datas[$key])) ? $this->datas[$key] : null;
    }

    public function set($key, $value)
    {
        $this->datas[$key] = $value;

        return $this;
    }

    public function getDatas()
    {
        return $this->datas;
    }

    /**
     * @param array $datas
     */
    public function setDatas($datas)
    {
        $this->datas = $datas;
    }
}