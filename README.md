# MVC

Ce projet est un MVC (Model View Controller) déjà prêt, simple a utiliser, et fait pour être le plus complet possible pour un petit projet web en PHP.

## RewriteRule (Reécriture d'url)

Le projet est basé sur la reécriture des URL, pour une meilleurs présentation, rappel : au lieu d'écrire `index.php?module=User&action=Info&id=12` l'url sera écrite comme ça -> `User/Info/id/12`. Mais pour que cela fonctionne, vous devez au préalable avoir activé sur votre serveur (ex : Apache), la reécriture d'url. Sinon vous aurez une erreur de serveur vous indiquant que le lien recherché n'existe pas.

Voici [ici](https://httpd.apache.org/docs/2.4/fr/mod/mod_rewrite.html) un tutoriel pour activer le Rewrite Engine sur un serveur Web Apache.

## Configuration

Deux fichiers de configurations .ini sont présent, un pour le développement et l'autre pour la production (`Config/dev.ini` et `Config/prod.ini`), ils servent à saisir des informations importantes à l'application tels que les informations de connexion à la base de données, la racine du site web, la page par défaut du site, etc ...

La classe `Core/Configuration.php` est là pour acceder aux informations des fichiers de configuration, voici un exemple d'utilisation :
```php
/*
 * Ceci est un extrait de la fonction qui sert à récupérer les informations pour la connexion à la BDD
 * La fonction get récupère les valeurs dans les fichiers .ini grâce aux clées passées en paramètres dans fonction get()
 */

$dsn   = Configuration::get("dsn");
$login = Configuration::get("login");
$pwd   = Configuration::get("pwd");
```

## Autoloader

Un Autoloader a été utilisé pour charger les classes PHP, afin d'éviter les `include` dans chaque fichier. La classe `Autoloader.php` a été récupéré sur un repository Git voir [ici](https://github.com/Nilpo/autoloader) la documentation. Cet autoloader fonctionne en faisant un appel dans le fichier `index.php` comme ceci `require_once 'Autoloader.php';`. 

## Smarty - moteur de template 

Afin d'éviter d'avoir du PHP avec de la syntaxe alternative `<?php if(...) ?>`, un moteur de template a été utilisé, qui est le [Smarty](https://www.smarty.net/).

Ce moteur de template utilise des fichiers `.tpl` qui doivent se trouver dans les dossiers `Views/` des Modules exemple : `Modules/Home/Views/index.tpl` ses fichiers .tpl seront compilés par les Classes Smarty (la librairie Smarty se trouve dans le dossier `Libraries/Smarty`). Les fichiers compilés seront ensuites dans `Smarty/templates_c` (cela ne sert à rien de les commit, se sont des fichiers générés dynamiquement, ils seront écrasé à chaque modification des fichiers .tpl).

## Traductions

Pour l'affichage du texte il est conseillé, d'utiliser les traductions, et de ne pas mettre de chaine de caractères traduite dans le code. Pour cela il existe 2 dossiers exemples pour le Français et l'Anglais, il se trouvent dans le dossier `Language/en` ou `Language/fr`.

Pour ajouter une chaine de caractères vous devez l'insérer dans le tableau `$string_values` présent dans les fichiers `string.php` qui sont dans chaque dossier de langue au format `"LBL_STRING_KEY" => "String value."`. Et ensuite vous devez faire appel à la fonction `t()` pour récupérer votre chaine de caractère exemple : `t("LBL_STRING_KEY")`. Le choix de la langue se fait en donnant une valeur à `$_SESSION['language']` exemple de valeur `fr`pour le Français ou `en` pour l'Anglais, vous pouvez rajouter d'autres langues.

## Business Class

Le dossier `Businness/` doit contenir les classes métiers, qui pourront par exemple servire à transformer vos résultats SQL en tableau d'objets (voir la fonction `Model::fetchToObject()`). Il est conseillé que les classes métiers hérites de la classe `Core/Core.php`.

## Model Class

La classe Model intéragit avec la base de données. Les fichiers doivents êtres mis dans le dossier `Models/` et hériter de la classe `Core/Model.php`, il est conseillé d'utiliser les classes Model uniquement dans les Controleurs. Il est conseillé d'initialiser le model dans le constructeur du controleur utilisé et d'utiliser le model comme donnée membre du controleur.

## View Class

Les vues sont crées dynamiquement lors de l'appel du controleur par le routeur, si une vue existe pour l'action demandé par l'utilisateur. Si le controleur a réussit a créer la vue, elle sera comme donnée membre de la classe `$this->view`.

Voici un exemple :
```php
// index() est une action du controleur 
protected function index()
{
    // création d'une variable 'cars' qui sera accessible dans le template
    $this->view->assign('cars', $this->carModel->getCars());
    // génère le fichier .tpl et l'affiche à l'utilisateur
    $this->view->postView();
}
```
Il existe ausssi une méthode `View::postAjaxView()` qui sert à afficher une vue sans le gabarit (header + footer `Modules/Utils/template.tpl`)

## URL

L'url doir contenir, le module et l'action a exécuter `index.php?module=Home&action=index`, ou sinon grâce à la réecriture d'url, il est possible de l'écrire comme ça `Home/index`