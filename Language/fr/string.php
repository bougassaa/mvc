<?php

$string_values = [
    "LBL_NO_ACTION" => "Action manquante.",
    "LBL_NO_CONTROLLER" => "Controleur demandé non existant.",
    "LBL_NO_MODULE" => "Module manquant.",
    "LBL_ACTION_NOT_EXIST" => "Action non existante.",
    "LBL_NO_VIEW" => "Aucune vue disponible.",
    "LBL_NO_CONFIG_FILE" => "Aucun fichier de configuration trouvé",
    "LBL_CONNECTION_FAILED" => "Connexion échouée : ",
    "LBL_GO_BACK" => "Cliquez pour retourner en arrière.",
    "LBL_TITLE_ERROR" => "Erreur",
    "LBL_TITLE_HOME" => "Accueil",
    "LBL_SEARCH" => "Chercher",
    "LBL_LANGUAGE" => "Langue"
];